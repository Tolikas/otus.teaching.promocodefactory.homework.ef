﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodesRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(IRepository<PromoCode> promocodesRepository, 
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _promocodesRepository = promocodesRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodesRepository.GetAllAsync();

            var promocodesResponse = promocodes.Select(p =>
                new PromoCodeShortResponse
                {
                    Id = p.Id,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    BeginDate = p.BeginDate.ToString("dd-MM-yyyy"),
                    EndDate = p.EndDate.ToString("dd-MM-yyyy"),
                    PartnerName = p.PartnerName
                }).ToList();

            return promocodesResponse;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            // Дополнительно проверим, есть ли уже такой промокод

            var alreadyExists = (await _promocodesRepository.GetAllAsync())
                .AsQueryable()
                .Any(p => p.Code == request.PromoCode);

            if (alreadyExists)
            {
                return BadRequest();
            }

            // В GivePromoCodeRequest нет свойства PartnerManager !

            var id = Guid.NewGuid();

            var preference = (await _preferenceRepository.GetAllAsync())
                .AsQueryable()
                .FirstOrDefault(p => p.Name == request.Preference);

            var promocode = new PromoCode
            {
                Id = id,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(1),
                PartnerName = request.PartnerName,
                Preference = preference
            };

            await _promocodesRepository.CreateAsync(promocode);

            // Промокод будем назначать не всем клиентам с таким предпочтением,
            // а только одному, поскольку это задано условиями пункта 4

            var customer = (await _customerRepository.GetAllAsync())
                .AsQueryable()
                .FirstOrDefault(p => p.Preferences.Select(n => n.Name).Contains(request.Preference));

            if (customer != null)
            {
                promocode = await _promocodesRepository.GetByIdAsync(id);

                var promocodes = customer.PromoCodes ?? new List<PromoCode>();
                promocodes.Add(promocode);

                customer.PromoCodes = promocodes;

                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}