﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            var useMigration = true;

            if (useMigration)
            {
                Database.Migrate();
            }
            else
            {
                Database.EnsureDeleted();
                Database.EnsureCreated();
            }

            var dataInitialization = !Roles.Any();

            if (dataInitialization)
            {
                AddRange(FakeDataFactory.Roles);
                SaveChanges();

                var employees = FakeDataFactory.Employees.ToList();

                foreach (var employee in employees)
                {
                    employee.Role = Roles.FirstOrDefault(p => p.Id == employee.Role.Id);
                }

                AddRange(employees);
                SaveChanges();

                AddRange(FakeDataFactory.Preferences);
                SaveChanges();

                var customers = FakeDataFactory.Customers.ToList();

                foreach (var customer in customers)
                {
                    customer.Preferences = Preferences.ToList();
                }
                
                AddRange(customers);
                SaveChanges();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Employees

            modelBuilder.Entity<Employee>()
                .HasOne(p => p.Role)
                .WithOne()
                .HasForeignKey<Employee>("RoleId")
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Employee>().HasIndex("RoleId").IsUnique(false);

            #endregion

            #region CustomerPreferences

            modelBuilder.Entity<Customer>()
                .HasMany(p => p.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<Dictionary<string, object>>(
                    "CustomerPreferences",
                    j => j
                        .HasOne<Preference>()
                        .WithMany()
                        .HasForeignKey("PreferenceId"),
                    j => j
                        .HasOne<Customer>()
                        .WithMany()
                        .HasForeignKey("CustomerId"));

            #endregion

            #region PromoCodes

            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(p => p.PromoCodes)
                .HasForeignKey("CustomerId")
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Инициализация данными (неработающая)

            //modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            //modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            //modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            //modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);

            #endregion

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #region Без этого не срабатывает миграция

            if (!optionsBuilder.IsConfigured)
            {
                // как прочитать из appsettings ?
                var connectionString = "Data Source=PromoCodeFactory.db";

                //IConfigurationRoot configuration = new ConfigurationBuilder()
                //    .SetBasePath(Directory.GetCurrentDirectory())
                //    .AddJsonFile("appsettings.json")
                //    .Build();

                //connectionString = configuration.GetConnectionString("DefaultConnection");

                optionsBuilder.UseSqlite(connectionString);
            }

            #endregion
        }

    }
}