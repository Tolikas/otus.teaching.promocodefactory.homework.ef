﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(p => p.Id == id));
        }

        public Task CreateAsync(T entity)
        {
            var changedData = Data.ToList();
            changedData.Add(entity);
            return Task.FromResult(Data = changedData);
        }

        public Task UpdateAsync(T entity)
        {
            var changedData = Data.ToList();
            var index = changedData.FindIndex(p => p == entity);
            changedData[index] = entity;
            return Task.FromResult(Data = changedData);
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.FromResult(Data = Data.Where(p => p.Id != id));
        }
    }
}